# Recursive Descent Parser

Recursive descent parser for a subset of Bash. Validates the Bash script, and presents an AST for the Bash script,
either to stdout or to a file, depending on the command-line arguments.

## Building and running

In order to run the application, you will need to build it with the bundled Gradle instance. To do so, run `./gradlew
clean build` in the `recursivedescent` directory. You should get the following output:

```
BUILD SUCCESSFUL in 0s
3 actionable tasks: 3 executed
```

If you get any build errors, ensure you are connected to the internet and the repository was cloned correctly, then try
again.

Once the jar has been built, you can run it directly with:

```
java -jar build/libs/recursivedescent-1.0-SNAPSHOT.jar [command-line args]
```

Alternatively, you can use the included `compile2C ` script, which will also build the project for you if you do not do
so manually. This document will assume you're using the included script.

## Usage

```
Usage: compile2C (-d|-p) filename.sh
```

To use the parser, you must supply two command-line arguments. The first tells whether the parser should output the AST
to stdout, or if it should output to a file. The legal flags are `-d` and `-p`. `-d` will output to stdout.

`-p` will output to a file that has the same name as the input shell script, but with a `.ast` extension. So, if the
input filename is `test.sh`, then the output filename will be `test.ast`. The output file will be in the same directory
as the input file. If the output file already exists, it will not be made, and an error will be shown in the console.

## List of valid Bash commands

The parser only supports a subset of Bash commands. They are:

- cat
- ls
- pwd
- touch
- cp
- mv
- rm
- chmod
- man
- ps
- bg
- mkdir
- cd
- test