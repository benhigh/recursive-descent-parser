package xyz.benhigh.compilertheory.recursivedescent.table;

import lombok.AllArgsConstructor;
import lombok.Data;
import xyz.benhigh.compilertheory.recursivedescent.ast.Command;

/**
 * Data class representing an entry in the identification table. Holds the scope level, identity, type, and a link to
 * the declaration, which can either be a ForCmd or an AssignCmd.
 *
 * @author Ben High
 * @version 1.0
 */
@Data
@AllArgsConstructor
public class IDTEntry {
    private int level;
    private String identity;
    private Type type;
    private Command declaration;
}
