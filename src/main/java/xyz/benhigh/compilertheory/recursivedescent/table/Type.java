package xyz.benhigh.compilertheory.recursivedescent.table;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enum for the type. Used for either an AssignCmd, or an argument.
 *
 * @author Ben High
 * @version 1.0
 */
@Getter
@AllArgsConstructor
public enum Type {
    NUMERIC("Numeric"),
    STRING("String"),
    EXECUTABLE("Executable");

    private String name;
}
