package xyz.benhigh.compilertheory.recursivedescent.table;

import xyz.benhigh.compilertheory.recursivedescent.ast.Command;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Identification table class. Contains methods for adding and retrieving entries from the table, as well as modifying
 * the current scope level.
 *
 * @author Ben High
 * @version 1.0
 */
public class IdentificationTable {
    private int currentLevel;
    private List<IDTEntry> entries;

    /**
     * Default constructor. Just sets the initial level and initializes the entries array.
     */
    public IdentificationTable() {
        currentLevel = 1;
        entries = new ArrayList<>();
    }

    /**
     * Create a new scope level.
     */
    public void addLevel() {
        currentLevel++;
    }

    /**
     * Delete all entries on the current scope level and close it.
     */
    public void remLevel() {
        entries.removeIf(o -> o.getLevel() == currentLevel);
        currentLevel--;
    }

    /**
     * Add an entry to the current scope level.
     *
     * @param ident      The name of the variable.
     * @param type       The type of the variable.
     * @param assignment A link to the command at which the variable was declared. Could be a ForCmd or an AssignCmd.
     */
    public void addEntry(String ident, Type type, Command assignment) {
        entries.add(new IDTEntry(currentLevel, ident, type, assignment));
    }

    /**
     * Retrieve an entry from the identification table. In the case of two entries with the same name at different scope
     * levels, this will return the entry that is in the deepest scope. Will return null if the variable does not exist
     * at any scope level.
     *
     * @param ident The name of the variable to retrieve.
     * @return An IDTEntry object of the variable requested, if it exists, otherwise null.
     */
    public IDTEntry getEntry(String ident) {
        Optional<IDTEntry> idtEntryOptional = entries.stream().filter(o -> o.getLevel() <= currentLevel &&
                o.getIdentity().equals(ident)).max(Comparator.comparingInt(IDTEntry::getLevel));

        return idtEntryOptional.orElse(null);
    }
}
