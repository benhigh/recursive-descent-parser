package xyz.benhigh.compilertheory.recursivedescent;

import xyz.benhigh.compilertheory.recursivedescent.ast.Command;

import java.io.*;
import java.util.Arrays;

public class compile2C {
	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_GREEN = "\u001B[32m";

	/**
	 *
	 *
	 * @param args
	 */
	private compile2C(String[] args) {
		// Parse the command-line arguments and print the usage if they are invalid
		CommandLineArgs options = parseArgs(args);
		if (options == null) {
			System.err.println("Usage: compile2C (-d|-p) filename.sh");
			return;
		}

		// Try to open the file
		File file = new File(options.getInputFile());
		if (!file.exists()) {
			System.err.println("File " + options.getInputFile() + " does not exist.");
			return;
		}

		// Read the file contents to a string
		StringBuilder builder = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				builder.append(line).append(" \n ");
			}
			br.close();
		} catch (IOException e) {
			System.err.println(e.getMessage() + " when reading file.");
			e.printStackTrace();
			return;
		}

		// Feed the data into the parser
		Parser parser = new Parser(builder.toString());

		// Feed the data into the contextual analyzer, if it passes parsing
		Visitor visitor = new Visitor();
		if (parser.isValid()) {
			visitor.visitScript(parser.getScript());
		}

		boolean produceAst = parser.isValid() && visitor.isValid();

		// Print a summary to stdout
		System.out.println("===========================");
		System.out.println("Syntactic analysis:  " + (parser.isValid() ? ANSI_GREEN + "PASSED" : ANSI_RED + "FAILED") + ANSI_RESET);
		System.out.println("Contextual analysis: " + (visitor.isValid() ? ANSI_GREEN + "PASSED" : ANSI_RED + "FAILED") + ANSI_RESET);
		System.out.println("===========================\n");

		if (!produceAst) {
			System.out.println(ANSI_RED + "Will not produce AST as analysis failed." + ANSI_RESET);
		}

		// Drop out now if the script is invalid
		if (!produceAst) {
			return;
		}

		// Get the AST if the script was parsed successfully, and output it to the appropriate place
		String ast = visitor.getAstString();
		if (options.getOutputType() == CommandLineArgs.FILE) {
			// Determine the output filename
			String[] filenameSplit = options.getInputFile().split("\\.");
			String outputFilename = String.join(".", Arrays.asList(filenameSplit).subList(0, filenameSplit.length - 1)) + ".ast";

			// Make sure the output file doesn't already exist
			File outputFile = new File(outputFilename);
			if (outputFile.exists()) {
				System.err.println("Output file \"" + outputFilename + "\" already exists.");
				return;
			}

			// Try to output to the file
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
				bw.write(ast);
				bw.flush();
				bw.close();
			} catch (IOException e) {
				System.err.println(e.getMessage() + " when writing to output file.");
				return;
			}

			System.out.println("Output written to \"" + outputFilename + "\"");
		} else if (options.getOutputType() == CommandLineArgs.STDOUT) {
			System.out.println(ast);
		}
	}

	/**
	 * Parse the command-line arguments into a CommandLineArgs object. If the command line arguments cannot be parsed,
	 * return null.
	 *
	 * @param args The array of command-line arguments.
	 * @return A CommandLineArgs object, if the arguments are correctly formed. Otherwise, null
	 */
	private CommandLineArgs parseArgs(String[] args) {
		// Enforce there's at least 1 command-line argument
		if (args.length < 1) {
			return null;
		}

		// If just the filename is there, then set to no output
		if (args.length == 1) {
			CommandLineArgs commandLineArgs = new CommandLineArgs();
			commandLineArgs.setInputFile(args[0]);
			commandLineArgs.setOutputType(CommandLineArgs.NO_OUTPUT);
			return commandLineArgs;
		}

		// Parse the flag argument
		CommandLineArgs commandLineArgs = new CommandLineArgs();
		if (args[0].equals("-p")) {
			commandLineArgs.setOutputType(CommandLineArgs.FILE);
		} else if (args[0].equals("-d")) {
			commandLineArgs.setOutputType(CommandLineArgs.STDOUT);
		} else {
			return null;
		}

		// Set the input file
		commandLineArgs.setInputFile(args[1]);
		return commandLineArgs;
	}

	/**
	 * Application entry point. Just initializes this class.
	 *
	 * @param args Command-line arguments to the JVM
	 */
	public static void main(String[] args) {
		new compile2C(args);
	}
}
