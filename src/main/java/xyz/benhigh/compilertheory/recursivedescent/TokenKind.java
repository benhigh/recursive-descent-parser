package xyz.benhigh.compilertheory.recursivedescent;

public enum TokenKind {
	ASSIGN("="),
	IF("if"),
	THEN("then"),
	ELSE("else"),
	FI("fi"),
	FOR("for"),
	IN("in"),
	DO("do"),
	OD("od"),
	FILENAME("<filename>"),
	LITERAL("<literal>"),
	VARIABLE("<variable>"),
	EOL("<end of line>"),
	EOT("<end of text>");

	private String spelling;

	TokenKind(String spelling) {
		this.spelling = spelling;
	}

	public String getSpelling() {
		return spelling;
	}
}
