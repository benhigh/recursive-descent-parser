package xyz.benhigh.compilertheory.recursivedescent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Token scanner. This class will parse all of the tokens in a clear text script, and hold a list of tokens. These
 * tokens can be collected one at a time by calling scan().
 *
 * @author Ben High
 * @version 1.0
 */
public class Scanner {
	private java.util.Scanner scanner;
	private List<Token> tokens;
	private int index;

	/**
	 * Construct the Scanner. Build a list of tokens by parsing through the string. Any parse errors will be thrown here
	 * if the scanner cannot understand a token.
	 *
	 * @param script The text of the script to be parsed
	 */
	public Scanner(String script) {
		scanner = new java.util.Scanner(script);
		tokens = new ArrayList<>();
		index = 0;

		List<String> filenames = Arrays.asList(
				"cat", "ls", "pwd", "touch", "cp",
				"mv", "rm", "chmod", "man", "ps", "bg",
				"mkdir", "cd", "test");
		int position = 0;
		scanner.useDelimiter(" ");

		while (scanner.hasNext()) {
			position++;
			String next = scanner.next();

			// Parse the token
			if (next.equals("")) { // Throw out blank tokens (indentation)
				continue;
			} else if (next.equals("=")) {
				tokens.add(new Token(TokenKind.ASSIGN));
			} else if (next.equals("if")) {
				tokens.add(new Token(TokenKind.IF));
			} else if (next.equals("then")) {
				tokens.add(new Token(TokenKind.THEN));
			} else if (next.equals("else")) {
				tokens.add(new Token(TokenKind.ELSE));
			} else if (next.equals("fi")) {
				tokens.add(new Token(TokenKind.FI));
			} else if (next.equals("for")) {
				tokens.add(new Token(TokenKind.FOR));
			} else if (next.equals("in")) {
				tokens.add(new Token(TokenKind.IN));
			} else if (next.equals("do")) {
				tokens.add(new Token(TokenKind.DO));
			} else if (next.equals("od")) {
				tokens.add(new Token(TokenKind.OD));
			} else if (next.equals("\n")) {
				tokens.add(Token.EOL);
			} else if (filenames.contains(next)) {
				tokens.add(new Token(TokenKind.FILENAME, next));
			} else if (Pattern.matches("[a-zA-Z]([A-z]|\\d|_|[.])*", next)) {
				tokens.add(new Token(TokenKind.VARIABLE, next));
			} else if (Pattern.matches("(-(-?)([a-zA-Z]|\\d)*)|\\d+", next)) {
				tokens.add(new Token(TokenKind.LITERAL, next));
			} else {
				System.err.println("Parse error: Found \"" + next + "\" at position " + position + ".");
				return;
			}
		}

		// Add an EOL and EOT token at the end, to finish things off
		tokens.add(Token.EOL);
		tokens.add(Token.EOT);
	}

	/**
	 * Get the next token in the script.
	 *
	 * @return The next token in the script.
	 */
	public Token scan() {
		return tokens.get(index++);
	}

	/*private static void test(int num, String script, boolean expected) {
		Parser parser = new Parser(script);

		System.out.print("**** TEST #" + num + " - ");
		if (expected == parser.isValid()) {
			System.out.println("PASSED");
		} else {
			System.out.println("FAILED");
		}
	}

	public static void main(String[] args) {
		test(1, "ls -al", true);
		test(2,"mv this that", true);
		test(21, "mv this that the other", true);
		test(22, "mv this that ok", true);
		test(3, "touch myNewFile \n mv myNewFile myStuff \n cd newStuff \n chmod 557", true);
		test(4, "if test -e cal.txt then \n cat cal.txt \n else \n ls -l \n fi", true);
		test(5, "for apples in fruit \n do \n mv apples basket \n od", true);
		test(6, "if test -e apples then \n else \n touch apples \n fi \n mkdir basket \n fruit = apples \n for file in fruit \n do \n mv file basket \n od", true);
		test(9, "if test -e apples then \n mkdir apples \n mv apples basket \n else \n fi", true);
		test(7, "notvalid", false);
	}*/
}