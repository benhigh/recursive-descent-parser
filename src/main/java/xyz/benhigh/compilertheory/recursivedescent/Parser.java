package xyz.benhigh.compilertheory.recursivedescent;

import xyz.benhigh.compilertheory.recursivedescent.ast.*;

import java.util.ArrayList;
import java.util.List;

public class Parser {
	private Token currentToken;
	private Scanner scanner;
	private boolean valid;
	private Script script;

	public Parser(String script) {
		scanner = new Scanner(script);
		currentToken = scanner.scan();
		valid = true;
		this.script = parseScript();
	}

	/**
	 * Accept a specified token if it matches the current Token.  Acceptance entails setting currentToken to the next
	 * token in the input stream.
	 *
	 * @param expectedKind The expected type of token.
	 */
	private void accept(TokenKind expectedKind) {
		if (currentToken.getKind() == expectedKind)
			currentToken = scanner.scan();
		else
			writeError("Expected: " + expectedKind.getSpelling() +
					"\t Found: " + currentToken.getKind().getSpelling());
	}

	/**
	 * Accept the current token by setting currentToken to the next token in the input stream.
	 */
	private void acceptIt() {
		currentToken = scanner.scan();
	}

	/**
	 * Write an error out to stderr and mark the script as invalid.
	 *
	 * @param s The string to be written to stderr.
	 */
	private void writeError(String s) {
		System.err.println(s);
		valid = false;
	}

	/**
	 * Parse the script as a whole. This will do the syntactic analysis and build the AST, setting the AST to the class
	 * "script" attribute, and returning it. This will also set the class "valid" attribute to false if the script is
	 * not valid, and will throw any parse errors found into stderr.
	 *
	 * @return The AST for the entire script.
	 */
	private Script parseScript() {
		Script script = new Script();
		script.setCommand(parseCommands());

		// Final validation
		if ((currentToken.getKind() != TokenKind.EOT && currentToken.getKind() != TokenKind.EOL) || !valid) {
			valid = false;
		}

		return script;
	}

	/**
	 * Parse a set of commands, and either just return the command itself (if there's only one), return an empty command
	 * if there's nothing to be found, or return a chain of SeqCmd objects, populated with the appropriate levels of
	 * nesting and populated with all commands. This method should be called when 2 or more commands are possible.
	 *
	 * @return A Command object populated with the appropriate levels of SeqCmd nesting for how many commands there are.
	 */
	private Command parseCommands() {
		// Collect all of the commands
		List<Command> commands = new ArrayList<>();
		while (currentToken.getKind() == TokenKind.FILENAME
				|| currentToken.getKind() == TokenKind.VARIABLE
				|| currentToken.getKind() == TokenKind.IF
				|| currentToken.getKind() == TokenKind.FOR) {
			commands.add(parseCommand());
		}

		// Handle if there are 0 or 1 commands (EmptyCmd and just the first command, respectively)
		if (commands.size() == 0) {
			return new EmptyCmd();
		} else if (commands.size() == 1) {
			return commands.get(0);
		}

		// Build a SeqCmd tree for the sequential commands
		SeqCmd seqCmd = new SeqCmd();
		SeqCmd currentCmd = seqCmd;
		for (int i = 0; i < commands.size(); i++) {
			if (i < commands.size() - 1) {
				currentCmd.setCommand1(commands.get(i));
				if (i != commands.size() - 2) {
					SeqCmd newBranch = new SeqCmd();
					currentCmd.setCommand2(newBranch);
					currentCmd = newBranch;
				}
			} else {
				currentCmd.setCommand2(commands.get(i));
			}
		}
		return seqCmd;
	}

	/**
	 * Parse a single command. This method will determine the appropriate command type, and recursively parse it and its
	 * children, adding them to the AST. When parsing the entire script, parseCommands should be used instead.
	 *
	 * @return A single parsed Command AST object, and its children.
	 */
	private Command parseCommand() {
		Command command = new EmptyCmd();
		switch (currentToken.getKind()) {
			case FILENAME: {
				command = new ExecCmd();
				((ExecCmd) command).setFilename((FnameArg) parseArgument()); // Filename to run
				((ExecCmd) command).setArguments(parseArguments()); // Any parameters
				accept(TokenKind.EOL);
				break;
			}
			case VARIABLE: {
				command = new AssignCmd();
				((AssignCmd) command).setVariable((VariableArg) parseArgument()); // Variable to assign to
				accept(TokenKind.ASSIGN);
				((AssignCmd) command).setValue((SingleArg) parseArgument()); // Value to assign
				accept(TokenKind.EOL);
				break;
			}
			case IF: {
				command = new IfCmd();
				acceptIt();
				if (currentToken.getKind() == TokenKind.FILENAME) { // Filename to check if true in the if statement
					Terminal terminal = new Terminal(currentToken.getSpelling());
					FnameArg fnameArg = new FnameArg();
					fnameArg.setTerminal(terminal);
					((IfCmd) command).setFilename(fnameArg);
					acceptIt();
				} else {
					writeError("Filename expected. Got " + currentToken.getKind().getSpelling() + ".");
				}
				((IfCmd) command).setArgument(parseArguments()); // If statement arguments
				accept(TokenKind.THEN);
				accept(TokenKind.EOL);
				((IfCmd) command).setThenCommand(parseCommands()); // Then block
				accept(TokenKind.ELSE);
				accept(TokenKind.EOL);
				((IfCmd) command).setElseCommand(parseCommands()); // Else block
				accept(TokenKind.FI);
				accept(TokenKind.EOL);
				break;
			}
			case FOR: {
				command = new ForCmd();
				acceptIt();
				if (currentToken.getKind() == TokenKind.VARIABLE) { // The variable to assign to in the for loop
					Terminal terminal = new Terminal(currentToken.getSpelling());
					VariableArg variableArg = new VariableArg();
					variableArg.setTerminal(terminal);
					((ForCmd) command).setVariable(variableArg);
					acceptIt();
				} else {
					writeError("Variable expected. Got " + currentToken.getKind().getSpelling() + ".");
				}
				accept(TokenKind.IN);
				((ForCmd) command).setArgument(parseArguments()); // The collection to iterate over
				accept(TokenKind.EOL);
				accept(TokenKind.DO);
				accept(TokenKind.EOL);
				((ForCmd) command).setCommand(parseCommands()); // The command to execute
				accept(TokenKind.OD);
				accept(TokenKind.EOL);
				break;
			}
		}

		return command;
	}

	/**
	 * Parse multiple arguments into either a sequential argument tree (if there are multiple arguments), or just return
	 * the single argument itself. Scans until there are no more arguments.
	 *
	 * @return Either a sequential argument tree, when there are multiple arguments, or just the argument if there's
	 *         only one.
	 */
	private Argument parseArguments() {
		// Collect all of the arguments
		List<Argument> arguments = new ArrayList<>();
		while (currentToken.getKind() == TokenKind.FILENAME ||
				currentToken.getKind() == TokenKind.LITERAL ||
				currentToken.getKind() == TokenKind.VARIABLE) {
			arguments.add(parseArgument());
		}

		// Enforce that at least one argument is required
		if (arguments.isEmpty()) {
			writeError("At least one argument expected. Found nothing.");
			return null;
		}

		// Just return the first argument if there is only one
		if (arguments.size() == 1) {
			return arguments.get(0);
		}

		// Otherwise, build a sequential argument tree with all of the arguments
		SequentialArgument arg = new SequentialArgument();
		SequentialArgument currentArg = arg;
		for (int i = 0; i < arguments.size(); i++) {
			if (i < arguments.size() - 1) {
				currentArg.setArgument1(arguments.get(i));
				if (i != arguments.size() - 2) {
					SequentialArgument newBranch = new SequentialArgument();
					currentArg.setArgument2(newBranch);
					currentArg = newBranch;
				}
			} else {
				currentArg.setArgument2(arguments.get(i));
			}
		}

		return arg;
	}

	/**
	 * Parse an argument into the appropriate argument subtype.
	 *
	 * @return A subtype of Argument appropriate for the argument itself.
	 */
	private Argument parseArgument() {
		if (currentToken.getKind() == TokenKind.FILENAME) {
			Terminal terminal = new Terminal(currentToken.getSpelling());
			FnameArg arg = new FnameArg();
			arg.setTerminal(terminal);
			acceptIt();
			return arg;
		} else if (currentToken.getKind() == TokenKind.LITERAL) {
			Terminal terminal = new Terminal(currentToken.getSpelling());
			LiteralArg arg = new LiteralArg();
			arg.setTerminal(terminal);
			acceptIt();
			return arg;
		} else if (currentToken.getKind() == TokenKind.VARIABLE) {
			Terminal terminal = new Terminal(currentToken.getSpelling());
			VariableArg arg = new VariableArg();
			arg.setTerminal(terminal);
			acceptIt();
			return arg;
		} else {
			writeError("Argument expected. Found " + currentToken.getKind().getSpelling() + ".");
			return null;
		}
	}

	/**
	 * Get whether the script is valid.
	 *
	 * @return True if the script is valid. False if it isn't.
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Get the script AST. Should only be called if isValid() is true, otherwise an unpredictably broken/incomplete tree
	 * will be returned.
	 *
	 * @return The script's AST root.
	 */
	public Script getScript() {
		return script;
	}
}