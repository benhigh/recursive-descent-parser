package xyz.benhigh.compilertheory.recursivedescent;

import org.apache.commons.lang3.StringUtils;
import xyz.benhigh.compilertheory.recursivedescent.ast.*;
import xyz.benhigh.compilertheory.recursivedescent.table.IDTEntry;
import xyz.benhigh.compilertheory.recursivedescent.table.IdentificationTable;
import xyz.benhigh.compilertheory.recursivedescent.table.Type;

/**
 * Visitor class for the AST. This will generate the AST string to be produced, while also doing contextual analysis.
 * If the contextual analysis is successful, calling isValid() will return true. If any contextual analysis errors occur,
 * they will be logged to stderr. The AST string can be retrieved by calling getASTString().
 *
 * @author Ben High
 * @version 1.0
 */
public class Visitor {
    private StringBuilder stringBuilder;
    private int indentLevel;
    private IdentificationTable identificationTable;
    private boolean valid;
    private boolean assigning; // For scope checking

    /**
     * Constructor for the visitor. Just sets up the default values.
     */
    public Visitor() {
        this.stringBuilder = new StringBuilder();
        this.indentLevel = 0;
        this.identificationTable = new IdentificationTable();
        this.valid = true;
        this.assigning = false;
    }

    /**
     * Main entry point of the visitor. Takes a script and begins visiting its initial command.
     *
     * @param script The root of the AST, the script to start visiting.
     */
    public void visitScript(Script script) {
        stringBuilder.append("Script\n");
        visitCommand(script.getCommand());
    }

    /**
     * Method for visiting a command. Performs a series of actions depending on the type of the command.
     *
     * - AssignCmd: Either creates a new entry on the identification table, or redefines an existing entry. Declaration
     *              is done when either the identifier for the variable doesn't exist on the identification table, or
     *              an existing identifier is at a greater scope. Reassignment is performed when the identifier exists
     *              at the current scope, or a higher scope. Also assigns the type of the AssignCmd to the type of the
     *              value being assigned.
     * - ExecCmd: Very simple visit that simply adds the label to the AST tree, then visits the ExecCmd's children.
     * - ForCmd: Defines a new scope for the identification table, and defines a new variable at that scope, which is
     *           the loop's variable. Once the for loop's command is finished being visited, the scope is decremented.
     * - IfCmd: Also very simple visit that just adds the label to the AST tree and visits its children.
     * - SeqCmd: Visits its children, like IfCmd and ExecCmd.
     * - EmptyCmd: Just prints out the label to the AST string, since there are no children to be visited.
     *
     * @param command The command to begin visiting.
     */
    private void visitCommand(Command command) {
        if (command instanceof AssignCmd) {
            stringBuilder.append(addIndent())
                    .append("AssignCmd\n");
            indentLevel++;

            // Visit the target variable, but don't allow scope errors to be thrown
            assigning = true;
            visitArgument(((AssignCmd) command).getVariable());
            assigning = false;

            // Visit the value, allowing scope errors to be thrown
            visitArgument(((AssignCmd) command).getValue());

            // Perform the assignment
            AssignCmd ac = (AssignCmd) command;
            if (ac.getVariable().getType() != null) {
                // If the variable exists, change its type and declaration
                IDTEntry entry = identificationTable.getEntry(ac.getVariable().getTerminal().getSpelling());
                entry.setType(ac.getVariable().getType());
                entry.setDeclaration(ac);
            } else {
                // If the variable doesn't exist, then we need to add it to the identification table
                identificationTable.addEntry(ac.getVariable().getTerminal().getSpelling(), ac.getValue().getType(), ac);
            }

            // Set the type of the assignment command
            ((AssignCmd) command).setType(ac.getValue().getType());
            indentLevel--;
        } else if (command instanceof ExecCmd) {
            // Not much to do here, just print the command and its children
            stringBuilder.append(addIndent())
                    .append("ExecCmd\n");
            indentLevel++;
            visitArgument(((ExecCmd) command).getFilename());
            visitArgument(((ExecCmd) command).getArguments());
            indentLevel--;
        } else if (command instanceof ForCmd) {
            stringBuilder.append(addIndent())
                    .append("ForCmd\n");
            indentLevel++;

            // New scope-- add a level to the identification table
            identificationTable.addLevel();

            // Visit the target loop variable, but disallow scope errors to be thrown
            assigning = true;
            visitArgument(((ForCmd) command).getVariable());
            assigning = false;

            // Add the variable to the identification table
            ForCmd fc = (ForCmd) command;
            identificationTable.addEntry(fc.getVariable().getTerminal().getSpelling(), fc.getArgument().getType(), fc);

            // Visit the arguments, allowing scope errors this time
            visitArgument(((ForCmd) command).getArgument());

            // Visit the command, after which we can decrement the scope level
            visitCommand(((ForCmd) command).getCommand());
            identificationTable.remLevel();
            indentLevel--;
        } else if (command instanceof IfCmd) {
            // Not much to do here either
            stringBuilder.append(addIndent())
                    .append("IfCmd\n");
            indentLevel++;
            visitArgument(((IfCmd) command).getFilename());
            visitArgument(((IfCmd) command).getArgument());
            visitCommand(((IfCmd) command).getThenCommand());
            visitCommand(((IfCmd) command).getElseCommand());
            indentLevel--;
        } else if (command instanceof SeqCmd) {
            // Once again, not much here to do, just visit the children
            stringBuilder.append(addIndent())
                    .append("SeqCmd\n");
            indentLevel++;
            visitCommand(((SeqCmd) command).getCommand1());
            visitCommand(((SeqCmd) command).getCommand2());
            indentLevel--;
        } else if (command instanceof EmptyCmd) {
            // REALLY nothing much to do here, just print out we have an empty command
            stringBuilder.append(addIndent())
                    .append("EmptyCmd\n");
            indentLevel--;
        }
    }

    /**
     * Method to visit arguments. Sets the type as appropriate for the value. Acts differently depending on the type of
     * the argument:
     *
     * - FnameArg: Sets the type to EXECUTABLE, as it will always be that.
     * - LiteralArg: Uses Apache Commons StringUtils to determine whether or not the value is numeric, then sets the
     *               type appropriately.
     * - SequentialArgument: Simply visits the children. SequentialArguments do not have types, as the children may have
     *                       differing types.
     * - VariableArg: Performs a lookup to find the type. If the lookup returns no entry, either a scope error will be
     *                thrown, if not inhibited, or a null type will be set for the argument, as the argument is being
     *                used on the left hand of an assignment statement.
     *
     * @param argument The argument to visit.
     */
    private void visitArgument(Argument argument) {
        if (argument instanceof FnameArg) {
            stringBuilder.append(addIndent())
                    .append("FnameArg (")
                    .append(((FnameArg) argument).getTerminal().getSpelling())
                    .append(")\n");

            // This has to be an executable, so set its type
            argument.setType(Type.EXECUTABLE);
        } else if (argument instanceof LiteralArg) {
            stringBuilder.append(addIndent())
                    .append("LiteralArg (")
                    .append(((LiteralArg) argument).getTerminal().getSpelling())
                    .append(")\n");

            // Determine the type by value
            String spelling = ((LiteralArg) argument).getTerminal().getSpelling();
            if (StringUtils.isNumeric(spelling)) {
                argument.setType(Type.NUMERIC);
            } else {
                argument.setType(Type.STRING);
            }
        } else if (argument instanceof SequentialArgument) {
            stringBuilder.append(addIndent())
                    .append("SequentialArgument\n");
            indentLevel++;
            visitArgument(((SequentialArgument) argument).getArgument1());
            visitArgument(((SequentialArgument) argument).getArgument2());
            indentLevel--;
        } else if (argument instanceof VariableArg) {
            stringBuilder.append(addIndent())
                    .append("VariableArg (")
                    .append(((VariableArg) argument).getTerminal().getSpelling())
                    .append(")\n");

            // Determine type by lookup
            IDTEntry entry = identificationTable.getEntry(((VariableArg) argument).getTerminal().getSpelling());
            if (entry != null) {
                argument.setType(entry.getType());
            } else if (!assigning) {
                // Throw an error for an unassigned variable, if throwing scope errors isn't disallowed
                writeError("ERROR: Attempt to use variable \"" + ((VariableArg) argument).getTerminal().getSpelling() +
                        "\", which is not defined in this context.");
            }
        }
    }

    /**
     * Helper method to just return a string with the appropriate number of spaces, based on the indentation level.
     *
     * @return A string of spaces to be put at the beginning of an AST string line.
     */
    private String addIndent() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= indentLevel; i++) {
            stringBuilder.append("  ");
        }
        return stringBuilder.toString();
    }

    /**
     * Write an error to stderr and mark the script as invalid.
     *
     * @param error The error message to write.
     */
    private void writeError(String error) {
        valid = false;
        System.err.println(error);
    }

    /**
     * Return the string representation of the AST.
     *
     * @return The string representation of the AST.
     */
    public String getAstString() {
        return stringBuilder.toString();
    }

    /**
     * Return whether or not the contextual analysis was successful.
     *
     * @return True if the script passed contextual analysis, false otherwise.
     */
    public boolean isValid() {
        return valid;
    }
}
