package xyz.benhigh.compilertheory.recursivedescent;

import lombok.Getter;
import lombok.Setter;

/**
 * Data class to hold the command line options for the application.
 *
 * @author Ben High
 * @version 1.0
 */
@Getter
@Setter
class CommandLineArgs {
	public static int NO_OUTPUT = 0;
	public static int STDOUT = 1;
	public static int FILE = 2;

	private int outputType;
	private String inputFile;
}
