package xyz.benhigh.compilertheory.recursivedescent;

import lombok.Data;

/**
 * Token object. Contains the kind of the token, and the literal spelling.
 */
@Data
public class Token {
	public static Token EOL = new Token(TokenKind.EOL, "");
	public static Token EOT = new Token(TokenKind.EOT, "");

	private TokenKind kind;
	private String spelling;

	public Token(TokenKind kind, String spelling) {
		this.kind = kind;
		this.spelling = spelling;
	}

	public Token(TokenKind kind) {
		this.kind = kind;
		this.spelling = "";
	}
}
