package xyz.benhigh.compilertheory.recursivedescent.ast;

/**
 * Abstract Command class for the different types of commands. Cannot be instantiated, but the different types of
 * commands should extend this one.
 *
 * @author Ben High
 */
public abstract class Command extends AST {
}
