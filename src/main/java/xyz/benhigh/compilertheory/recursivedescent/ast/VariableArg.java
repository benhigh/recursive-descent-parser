package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * Variable argument.
 *
 * @author Ben High
 */
@Getter
@Setter
public class VariableArg extends SingleArg {
	private Terminal terminal;

	/**
	 * Visit this node. Will just return that it is a VarArg, and the associated raw text.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return The string representation of this VarArg and its raw text
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "VarArg (" + terminal.getSpelling() + ")";
	}
}
