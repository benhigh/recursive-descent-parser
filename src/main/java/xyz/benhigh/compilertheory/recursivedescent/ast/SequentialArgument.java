package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * Sequential argument. Used when two or more arguments are wanted.
 *
 * @author Ben High
 */
@Getter
@Setter
public class SequentialArgument extends Argument {
	private Argument argument1;
	private Argument argument2;

	/**
	 * Visit this node. Will visit the two children of this SequentialArgument.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of this SequentialArgument and its descendents
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "SeqArg\n" +
				argument1.visit(indentLevel + 1) + "\n" +
				argument2.visit(indentLevel + 1);
	}
}
