package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * AST terminal. Should be the leaf of all trees.
 *
 * @author Ben High
 */
@Getter
@Setter
@AllArgsConstructor
public class Terminal extends AST {
	private String spelling;

	/**
	 * Visit this node. Will just print that it is a terminal, and the raw text associated with it. This visit method
	 * will likely not be called, since the args will just take the spelling directly.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return The string representation of this Terminal and its associated raw text
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "Terminal (" + spelling + ")";
	}
}
