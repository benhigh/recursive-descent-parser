package xyz.benhigh.compilertheory.recursivedescent.ast;

/**
 * Empty argument. Leads nowhere. Used for something like the "else" of IfArg when no command is wanted.
 *
 * @author Ben High
 */
public class EmptyCmd extends Command {
	/**
	 * Visit this node. Will just return "EmptyCmd," since there's nowhere else to go.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return The string "EmptyCmd" at the appropriate level of indentation
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "EmptyCmd";
	}
}
