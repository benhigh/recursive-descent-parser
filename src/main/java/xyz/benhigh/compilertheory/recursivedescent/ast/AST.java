package xyz.benhigh.compilertheory.recursivedescent.ast;

/**
 * Abstract class for all AST-related classes. Contains methods all nodes should have.
 *
 * @author Ben High
 */
public abstract class AST {
	/**
	 * Abstract method. Visit the node in the AST and subsequently any children, if applicable. Each subclass of AST
	 * should implement their own version of the visit method.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of any given node
	 */
	public abstract String visit(int indentLevel);

	/**
	 * Helper function for children to use to add a couple of spaces at the beginning of their visit strings, according
	 * to their indentation level (ie, how deep into the tree they are). 2 spaces are returned per indentation level,
	 * so the number of spaces will be 2*indentationLevel.
	 *
	 * @param amount The indentation level
	 * @return A string of spaces, according to the indentation level
	 */
	protected String addTabs(int amount) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < amount; i++) {
			sb.append("  ");
		}
		return sb.toString();
	}
}
