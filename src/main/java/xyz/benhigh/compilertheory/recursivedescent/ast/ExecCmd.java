package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * Execution command. Runs a filename and arguments.
 *
 * @author Ben High
 */
@Getter
@Setter
public class ExecCmd extends Command {
	private FnameArg filename;
	private Argument arguments;

	/**
	 * Visit this node. Will visit the filename and any arguments.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of this ExecCmd and its descendents
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "ExecCmd \n" +
				filename.visit(indentLevel + 1) + "\n" +
				arguments.visit(indentLevel + 1);
	}
}
