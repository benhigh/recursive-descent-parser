package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;
import xyz.benhigh.compilertheory.recursivedescent.table.Type;

/**
 * Abstract class for arguments.
 *
 * @author Ben High
 */
@Getter
@Setter
public abstract class Argument extends AST {
    private Type type;
}
