package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * Filename argument.
 *
 * @author Ben High
 */
@Getter
@Setter
public class FnameArg extends SingleArg {
	private Terminal terminal;

	/**
	 * Visit this node. Will return that it is a filename argument, and the raw terminal text.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of this FnameArg and its literal text
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "FnameArg (" + terminal.getSpelling() + ")";
	}
}
