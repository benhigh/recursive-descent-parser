package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;
import xyz.benhigh.compilertheory.recursivedescent.table.Type;

/**
 * Assignment command. Contains a variable argument and single argument.
 *
 * @author Ben High
 */
@Getter
@Setter
public class AssignCmd extends Command {
	private VariableArg variable;
	private SingleArg value;
	private Type type;

	/**
	 * Visit method for AssignCmd. Visit the variable and the value.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representing this AssignCmd and its descendents
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "AssignCmd \n" +
				variable.visit(indentLevel + 1) + "\n" +
				value.visit(indentLevel + 1);
	}
}
