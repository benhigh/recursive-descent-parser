package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * Literal argument.
 *
 * @author Ben High
 */
@Getter
@Setter
public class LiteralArg extends SingleArg {
	private Terminal terminal;

	/**
	 * Visit this node. Will print out that it's a literal argument, and the raw terminal text.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of this LiteralArg node and its raw terminal text
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "LitArg (" + terminal.getSpelling() + ")";
	}
}
