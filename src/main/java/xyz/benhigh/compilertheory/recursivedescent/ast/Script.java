package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * Script node of the tree. The root node.
 *
 * @author Ben
 */
@Getter
@Setter
public class Script extends AST {
	private Command command;

	/**
	 * Visit the script. Will dive down into the top level command and all of its descendants.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of the script's AST
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "Script \n" +
				command.visit(indentLevel + 1) + "\n";
	}
}
