package xyz.benhigh.compilertheory.recursivedescent.ast;

/**
 * Single argument. Used instead of Argument when only one argument is wanted.
 *
 * @author Ben High
 */
public abstract class SingleArg extends Argument {
}
