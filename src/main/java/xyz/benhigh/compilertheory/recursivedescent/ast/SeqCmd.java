package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * Class for sequential commands. Runs one command after another.
 *
 * @author Ben High
 */
@Getter
@Setter
public class SeqCmd extends Command {
	private Command command1;
	private Command command2;

	/**
	 * Visit this node. Will visit the commands attached to this SeqCmd.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of this SeqCmd and its associated children
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "SeqCmd \n" +
				command1.visit(indentLevel + 1) + "\n" +
				command2.visit(indentLevel + 1);
	}
}
