package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * If command. Contains a filename argument, a set of arguments, and two commands- one for the then, and one for the
 * else.
 *
 * @author Ben High
 */
@Getter
@Setter
public class IfCmd extends Command {
	private FnameArg filename;
	private Argument argument;
	private Command thenCommand;
	private Command elseCommand;

	/**
	 * Visit this node. Will visit the filename, all arguments, the then block and the else block.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of this IfCmd and its descendents
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "IfCmd \n" +
				filename.visit(indentLevel + 1) + "\n" +
				argument.visit(indentLevel + 1) + "\n" +
				thenCommand.visit(indentLevel + 1) + "\n" +
				elseCommand.visit(indentLevel + 1);
	}
}
