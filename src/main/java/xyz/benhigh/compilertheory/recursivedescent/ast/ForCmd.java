package xyz.benhigh.compilertheory.recursivedescent.ast;

import lombok.Getter;
import lombok.Setter;

/**
 * For command.
 *
 * @author Ben High
 */
@Getter
@Setter
public class ForCmd extends Command {
	private VariableArg variable;
	private Argument argument;
	private Command command;

	/**
	 * Visit this node. Will also visit the variable, argument, and command.
	 *
	 * @param indentLevel The indentation level of the node, for use with addTabs
	 * @return A string representation of this ForCmd and its descendents
	 */
	@Override
	public String visit(int indentLevel) {
		return addTabs(indentLevel) + "ForCmd \n" +
				variable.visit(indentLevel + 1) + "\n" +
				argument.visit(indentLevel + 1) + "\n" +
				command.visit(indentLevel + 1) ;
	}
}
